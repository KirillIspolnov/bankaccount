package org.kbff.edu.bankaccount;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class DebitorThreadTest {

	@Test
	public void testIncreasing() throws InterruptedException {
		BankAccount account = new BankAccount();
		DebitorThread debitor = new DebitorThread(account);
		debitor.start();
		Thread.sleep(1000);
		assertNotEquals(0, account.getBalance());
	}
}
