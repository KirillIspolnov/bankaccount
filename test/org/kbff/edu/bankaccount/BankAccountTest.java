package org.kbff.edu.bankaccount;

import static org.junit.Assert.*;

import org.junit.Test;

public class BankAccountTest {

	@Test
	public void testGetBalance() {
		BankAccount account = new BankAccount(100);
		assertEquals(100, account.getBalance());
	}

	@Test
	public void testIncrease() {
		BankAccount account = new BankAccount();
		account.increase(100);
		assertEquals(100, account.getBalance());
	}

	@Test
	public void testDecrease() {
		BankAccount account = new BankAccount(500);
		account.decrease(100);
		assertEquals(400, account.getBalance());
	}

}
