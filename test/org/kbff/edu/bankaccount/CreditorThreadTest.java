package org.kbff.edu.bankaccount;

import static org.junit.Assert.*;

import org.junit.Test;

public class CreditorThreadTest {

	@Test
	public void testDecreasingOnce() throws InterruptedException {
		BankAccount account = new BankAccount(2500);
		CreditorThread debitor = new CreditorThread(account);
		debitor.start();
		Thread.sleep(1000);
		assertNotEquals(2500, account.getBalance());
	}

	@Test
	public void testDecreasingCycle() throws InterruptedException {
		BankAccount account = new BankAccount(10_000);
		CreditorThread debitor = new CreditorThread(account);
		debitor.start();
		Thread.sleep(1000);
		assertNotEquals(10_000, account.getBalance());
	}
}
