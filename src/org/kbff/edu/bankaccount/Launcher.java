package org.kbff.edu.bankaccount;

public class Launcher {

	public static void main(String[] args) {
		BankAccount account = new BankAccount();
		
		DebitorThread debitor = new DebitorThread(account);
		CreditorThread creditor = new CreditorThread(account);
		
		debitor.start();
		creditor.start();
	}
}
