package org.kbff.edu.bankaccount;

/**
 * Поток-дебитор - пополняет некоторый банковский счёт несколькими зачислениями
 * Количество транзакций (зачислений) указано в константе {@link #TRANSACTIONS_LIMIT}
 * Сумма, зачисляемая на счёт за раз - {@link #MONEY_TRANSACTION}
 * После зачисления суммы на счёт выполняется вывод состояния счёта на экран и
 * пробуждение потока-кредитора
 *  
 * @author K. Ispolnov, 1618k
 */
public class DebitorThread extends Thread {
	private static final int TRANSACTIONS_LIMIT = 100;
	private static final int MONEY_TRANSACTION = 1_000;
	
	private BankAccount target;
	
	public DebitorThread(BankAccount target) {
		this.target = target;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < TRANSACTIONS_LIMIT; i++) {
			target.increase(MONEY_TRANSACTION);
			System.out.printf("Поступление средств: %d; Всего средств: %d\n",
						MONEY_TRANSACTION, target.getBalance());
		}
		
		target.close();
	}
}
