package org.kbff.edu.bankaccount;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Представляет банковский счёт, который имеет некоторый баланс
 * в виде целочисленного количества некоторой валюты. Над банковским
 * счётом можно совершать некоторые операции:
 * <ul>
 * 	<li>{@link #getBalance()} узнать баланс</li>
 *  <li>{@link #increase(int)} пополнить счёт на некоторую сумму</li>
 *  <li>{@link #decrease(int)} списать со счёта некоторую сумму</li>
 * </ul>
 * Счёт может находиться в двух состояниях:
 * <ul>
 * 	<li>открыт (зачисления происходят)</li>
 *  <li>закрыт (вас отчислили и зачисления не происходят)</li>
 * </ul>
 * 
 * @author K. Ispolnov, 1618k
 */
public class BankAccount {
	private AtomicInteger balance;
	private boolean opened;
	
	/**
	 * Инициализирует счёт как уже существующий с некоторой суммой на балансе
	 * 
	 * @param balance текущий баланс счёта
	 */
	public BankAccount(int balance) {
		this.balance = new AtomicInteger(balance);
		this.opened = true;
	}
	
	/**
	 * Инициализирует объект как пустой счет, баланс которого равен 0
	 */
	public BankAccount() {
		this(0);
	}
	
	/**
	 * Возвращает текущий баланс счёта
	 * 
	 * @return текущий баланс счёта
	 */
	public int getBalance() {
		return balance.get();
	}
	
	/**
	 * Увеличивает текущий баланс на указанную сумму
	 * 
	 * @param delta сумма к зачислению
	 */
	public void increase(int delta) {
		this.balance.addAndGet(delta);
	}
	
	/**
	 * Уменьшает текущий баланс на указанную сумму
	 * 
	 * @param delta сумма к списанию
	 */
	public void decrease(int delta) {
		this.balance.addAndGet(-delta);
	}
	
	/**
	 * Возвращает true, если счёт открыт
	 * Счёт считается открытм до тех пор, пока дебитор пополняет его (на самом деле пока дебитор не закроет этот счёт)
	 * 
	 * @return true, если счёт открыт
	 */
	public boolean isOpened() {
		return opened;
	}
	
	/**
	 * Закрывет счёт, присваивая полю {@link #opened} значение <code>false</code> 
	 */
	public void close() {
		opened = false;
	}
}
