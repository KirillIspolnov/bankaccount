package org.kbff.edu.bankaccount;

/**
 * Поток-кредитор, списывающий деньги с банковского счёта равными частями
 * Сумма списания за один раз указана в константе {@link #DECREASING_MINIMUM}
 * 
 * @author K.Ispolnov, 1618k
 */
public class CreditorThread extends Thread {
	private static final int DECREASING_MINIMUM = 2_500;
	
	private BankAccount target;
	
	public CreditorThread(BankAccount target) {
		this.target = target;
	}
	
	@Override
	public void run() {
		while(target.isOpened()) { 
			decreaseAllAvailable();
		};
	}
		
	/**
	 * Списывает с банковского счёта максимально возможную сумму
	 * Максимально возможная сумма - некоторая сумма, кратная значению {@link #DECREASING_MINIMUM} и имеющаяся на счету
	 */
	public void decreaseAllAvailable() {
		while(target.getBalance() >= DECREASING_MINIMUM) {
			target.decrease(DECREASING_MINIMUM);
			System.out.printf("Списание %d; Остаток: %d\n", DECREASING_MINIMUM, target.getBalance());
		}
	}
}
